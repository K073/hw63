import React from 'react';

const Task = (props) =>{
  return (
    <li className={"Task"}>
      {props.text}<button className={"Task-remove"} onClick={props.removeTask}>remove</button>
    </li>
  );
};

export default Task;