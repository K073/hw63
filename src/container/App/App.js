import React, { Component } from 'react';
import './App.css';
import {BrowserRouter, Link, Route, Switch} from "react-router-dom";
import FilmList from "../FilmList/FilmList";
import TodoList from "../TodoList/TodoList";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <nav>
            <Link to='/'>Film List</Link>
            <Link to='/todo'>TodoList</Link>
          </nav>
          <Switch>
            <Route path="/" exact component={FilmList}/>
            <Route path="/todo" component={TodoList}/>
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
