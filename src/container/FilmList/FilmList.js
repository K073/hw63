import React, {Component} from 'react';
import ListItem from "../../components/ListItem/ListItem";
import * as axios from "axios/index";

class FilmList extends Component {
  state = {
    filmList: [],
    value: ''
  };

  addFilm = event => {
    event.preventDefault();
    const value = this.state.value;
    axios.post('https://home-work63.firebaseio.com/film.json', {name: value}).then(res => {
      this.addFilmState(value, res.data.name);
    })
  };

  removeFilm = id => {
    axios.delete(`https://home-work63.firebaseio.com/film/${id}.json`).then(res => {
      const filmList = [...this.state.filmList];
      const index = filmList.map(value => value.id).indexOf(id);
      filmList.splice(index, 1);

      this.setState({filmList})
    });
  };

  inputHandler = event => {
    this.setState({value: event.target.value});
  };

  addFilmState = (value, id) => {
    const filmList = [...this.state.filmList];
    filmList.push({
      value: value,
      id: id
    });

    this.setState({filmList});
  };

  componentDidMount() {
    axios.get('https://home-work63.firebaseio.com/film.json').then(res => {
      for (let key in res.data) {
        this.addFilmState(res.data[key].name, key)
      }
    })
  }


  render() {
    return (
      <div>
        <form onSubmit={this.addFilm}>
          <input type="text" value={this.state.value} onChange={this.inputHandler} placeholder={'Enter Film Name'}/>
          <input type="submit" value="Add"/>
        </form>
        {this.state.filmList.length > 0 ? this.state.filmList.map((value) => <ListItem key={value.id}
                                                                                       click={() => this.removeFilm(value.id)}>{value.value}</ListItem>) :
          <p>add films!</p>}
      </div>
    );
  }
}

export default FilmList;