import React, { Component } from 'react';
import Task from "../../components/Task/Task";
import * as axios from "axios";

class TodoList extends Component {

  constructor (props) {
    super(props);
    this.state = {
      arrTask: [],
      inputValue: ''
    };
  }

  removeTask = (index) => {
    axios.delete(`https://home-work63.firebaseio.com/todo/${index}.json`).then(res => {
      const arrTask = [...this.state.arrTask]
      const keysArr = arrTask.map(value => value.key);
      arrTask.splice(keysArr.indexOf(index), 1);
      this.setState({arrTask});
      console.log(arrTask);
    });
  };

  getValue = (event) => {
    this.setState({inputValue : event.target.value});
  };

  addTask = (event) => {
    event.preventDefault();

    if (this.state.inputValue !== ''){
      this.addTaskInState(this.state.inputValue);
      axios.post('https://home-work63.firebaseio.com/todo.json', {name: this.state.inputValue}).then(res => {
        console.log(res);
      })
    }
  };

  addTaskInState = (value, key) => {
    const arrTask = [...this.state.arrTask];
    arrTask.push({'value': value ,'key': key});
    this.setState({arrTask});
  };

  componentDidMount () {
    axios.get('https://home-work63.firebaseio.com/todo.json').then(res => {
      for (let key in res.data){
        this.addTaskInState(res.data[key].name, key)
        console.log(key);
      }
    })
  }

  render() {
    let list = this.state.arrTask.map(value => {
      return <Task key={value.key} text={value.value} removeTask={() => this.removeTask(value.key)}/>
    });
    return (
      <div className="App">
        <form action="" onSubmit={this.addTask}>
          <input type="text" onChange={this.getValue}/>
          <input type="submit" value="add"/>
        </form>
        <ul className={"Task-list"}>{list}</ul>
      </div>
    );
  }
}

export default TodoList;